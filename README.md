# NOTICE
### This Project Was Migrated to author-gitlab.skills.network
#### Any changes you make here could result in either no effect or undesirable side effects.
#### Please Visit https://author-gitlab.skills.network/quicklabs/how-to-quicklab

# Skills Network QuickLab Sample Project (**change me**)

Please follow instructions in this [knowledge  base article](https://support.cognitivefaculty.com/knowledgebase/articles/1936807-how-to-create-a-quicklab-or-tutorial) to create your own project.

## Write instructions for your lab users to follow
While you can write your instructions in any file, we strogly recommend that you write them in *instructions.md*. Write your instructions in markdown. You can use the GitLab editor to do your work directly in the master branch. Just remember that every time you save your file you are committing it to master and this will trigger an upload to the Skills Network Asset Library. This means if your lab is live, every user will see your changes.

Do you pictures and even videos. Put those in to the *images* folder.



## Publish

If you followed the instructions in the [knowledge  base article](https://support.cognitivefaculty.com/knowledgebase/articles/1936807-how-to-create-a-quicklab-or-tutorial) to create your this project, GitLab CI will auto upload all content in this repository to the Skills Network Asset Library (implemented in the IBM Cloud Object Storage (COS) ) when you merge your files into `master` branch. All content will become publicly accessible from the Skills Network Asset Library. Please check out the [section](#section) below how to get the public url to the resource. In other words, **no action is required from you to publish your lab content**. It will be moved in to the Skills Netork Asset Library automatically. 

## Links to your content

To use your content you are going to need a publicly accessible url (link). This link is created every time you merge your content in to master branch and it points to your content in the Skills Network Asset Library. You will find links to every piece of your content in the GitLab Page for your project. You will find your project's GitLab Page by going to the `Settings > Pages` menu or by using the this url  `<https://ibm-skills-network.gitlab.io/quicklabs/<project_name>`.

For example the GitLab Page for the sample project is at `<https://ibm-skills-network.gitlab.io/quicklabs/sample-project/>`.
